import argparse
import os
import sys


from app.middleware import ServeiIdentificacioLocator, FrameworkAgentFactoryLocator
from app.middleware.aplicacio import AplicacioFactory


def llegir_parametres(serveis_disponibles, agents_disponibles):
    console_arg_parser = argparse.ArgumentParser(
        prog='Identificador',
        description='Servei remot d\'identificacio de plantes',
        epilog='Part del projecte CorreFulles')
    console_arg_parser.add_argument(
        '-i', '--identificador', required=True, choices=serveis_disponibles, type=str,
        dest='service', help='L\'API extern de reconeixement de plantes a utilitzar')
    console_arg_parser.add_argument(
        '-f', '--framework', required=True, choices=agents_disponibles, type=str,
        dest='agent', help='El framework a utilitzar per arrencar el servei')
    console_arg_parser.add_argument(
        '--port', type=int, default=8050,
        dest='port', help='El número de port per al servei')
    console_arg_parser.add_argument(
        '--host', type=str, default='0.0.0.0',
        dest='host', help='L\'adreça IP d\'acollida per al servei')
    console_arg_parser.add_argument(
        '--use-ssl', action='store_true',
        dest='ssl_enabled', help='Habilita l\'ús de HTTPS per al servei')

    parametres = console_arg_parser.parse_args()
    return parametres.__dict__


if __name__ == '__main__':
    f_locator = FrameworkAgentFactoryLocator()
    s_locator = ServeiIdentificacioLocator()
    app_factory = AplicacioFactory(f_locator, s_locator)
    parametres_programa = llegir_parametres(app_factory.ids_identificadors, app_factory.ids_frameworks)
    try:
        instancia_aplicacio = app_factory.crear_aplicacio(
            parametres_programa['agent'],
            parametres_programa['service'])

        entorn = os.environ.get('ENVIRONMENT', 'dev')
        instancia_aplicacio.run(
            host=parametres_programa['host'],
            port=parametres_programa['port'],
            debug=entorn != 'prod',
            ssl=parametres_programa['ssl_enabled']
        )
    except Exception as error:
        print(str(error), file=sys.stderr)
