from abc import ABC, abstractmethod
from typing import TypeVar


class ServeiRegistrable(ABC):

    @classmethod
    @abstractmethod
    def id_servei(cls) -> str:
        raise NotImplementedError(f"Service {cls.__name__} must implement method service_class_id.")


Servei = TypeVar('Servei', bound=ServeiRegistrable)
