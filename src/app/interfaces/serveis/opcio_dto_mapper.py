from abc import ABC, abstractmethod

from app.domain import OpcioIdentificacioDTO, OpcioIdentificacio


class InOpcioIdentificacioDTOMapper(ABC):
    @classmethod
    @abstractmethod
    def crea_dto_de_dades(cls, data, inclou_imatge: bool = False) -> OpcioIdentificacioDTO:
        raise NotImplementedError()


class OutOpcioIdentificacioDTOMapper(ABC):
    @classmethod
    @abstractmethod
    def crea_dto_de_opcio(cls, dto: OpcioIdentificacioDTO) -> OpcioIdentificacio:
        raise NotImplementedError()