from abc import abstractmethod
from typing import TypeVar, List

from app.domain import Foto, OpcioIdentificacioDTO
from .servei_registrable import ServeiRegistrable


class IServeiIdentificacio(ServeiRegistrable):

    @abstractmethod
    def troba_opcions(self, foto: Foto) -> List[OpcioIdentificacioDTO]:
        raise NotImplementedError


ServeiIdentificador = TypeVar('ServeiIdentificador', bound=IServeiIdentificacio)
