class ApiError(Exception):

    def __init__(self, message="Error a l'API d'identificació de plantes"):
        super().__init__(message)


class ControllerError(Exception):

    def __init__(self, message="Error del controlador"):
        super().__init__(message)


class FotoParserError(Exception):
    def __init__(self, message=""):
        super().__init__(f"No s'ha pogut processar la foto: {message}")


class FotoTruncadaError(FotoParserError):
    def __init__(self):
        super().__init__("Imatge truncada")


class FotoBuidaError(FotoParserError):
    def __init__(self, message="L'arxiu és buit"):
        super().__init__(message)


class ArxiuNoInclosError(ControllerError):
    def __init__(self, message="Arxiu no enviat"):
        super().__init__(message)


class FormatArxiuIncorrecteError(ControllerError):
    def __init__(self, format_arxiu: str):
        super().__init__(f"Format d'imatge invàlid: {format_arxiu}")


class OpcionsNoTrobadesError(ApiError):
    def __init__(self, message="No s'ha identificat cap espècie"):
        super().__init__(message)


class OpcionsMalParsejadesError(ApiError):
    def __init__(self, message=""):
        super().__init__(f"No s'ha pogut processar les respostes rebudes: {message}")


class TempsLimitExceditError(ApiError):
    def __init__(self, message="Error del client http: No s'ha pogut obtenir resultats dins del temps límit"):
        super().__init__(message)
