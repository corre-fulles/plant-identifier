from abc import ABC, abstractmethod
from typing import TypeVar

from app.interfaces.serveis import IServeiIdentificacio,ServeiRegistrable
from .wrapper import FrameworkAppWrapper
from app.middleware.controller import AbstractIdentificacioController


class FrameworkAgent(ABC):

    def __init__(self, wrapper: FrameworkAppWrapper, controller: AbstractIdentificacioController):
        self.app_wrapper = wrapper
        self.controlador = controller
        self._inicialitzar_rutes()

    @abstractmethod
    def _inicialitzar_rutes(self):
        raise NotImplementedError()

    @abstractmethod
    def iniciar(self, *args, **kwargs):
        raise NotImplementedError()


class AbstractFrameworkAgentFactory(ServeiRegistrable):

    @classmethod
    @abstractmethod
    def crear_agent(cls, servei_identificador: IServeiIdentificacio, *args, **kwargs) -> FrameworkAgent:
        raise NotImplementedError()


FrameworkAgentFactory = TypeVar('FrameworkAgentFactory', bound=AbstractFrameworkAgentFactory)
