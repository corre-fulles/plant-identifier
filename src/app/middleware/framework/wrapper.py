from abc import ABC, abstractmethod
from typing import Any


class FrameworkAppWrapper(ABC):

    def __init__(self, app: Any):
        self.app = app

    @abstractmethod
    def afegir_ruta(self, url: str, metodes: list, handler):
        raise NotImplementedError()

    @abstractmethod
    def run(self, *args, **kwargs):
        raise NotImplementedError()