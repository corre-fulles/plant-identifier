from abc import ABC, abstractmethod

from app.config.constants import FORMATS_VALIDS
from app.interfaces.serveis import IServeiIdentificacio


class AbstractIdentificacioController(ABC):

    def __init__(self, adaptador_api: IServeiIdentificacio):
        self.adaptador_api = adaptador_api

    def _es_format_valid(self, extensio: str):
        return extensio in FORMATS_VALIDS

    @abstractmethod
    def identificar_imatge(self, *args, **kwargs):
        raise NotImplementedError
