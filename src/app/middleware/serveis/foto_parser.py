import mimetypes
import uuid
from abc import ABC, abstractmethod

from app.domain import Foto
from app.middleware.errors import FotoBuidaError, FotoTruncadaError, ControllerError, FotoParserError


class FotoParser(ABC):
    @classmethod
    def foto_from_arxiu(cls, arxiu_imatge) -> Foto:
        try:
            extensio_amb_punt = mimetypes.guess_extension(arxiu_imatge.content_type)
            extensio_sense_punt = extensio_amb_punt[1:]
            nom = f'{uuid.uuid4()}{extensio_amb_punt}'
            bytes_imatge = cls._extreu_bytes(arxiu_imatge)
            if len(bytes_imatge) == 0:
                raise FotoBuidaError()
            return Foto(nom, bytes_imatge, extensio_sense_punt)
        except OSError as error_os:
            if str(error_os) == "truncated PNG file":
                raise FotoTruncadaError()
            else:
                raise FotoParserError(f"Error desconegut: {str(error_os)}")
        except FotoParserError as error_parseig:
            raise error_parseig
        except Exception as error:
            raise FotoParserError(f"Error desconegut: {str(error)}")
    @staticmethod
    @abstractmethod
    def _extreu_bytes(arxiu_imatge) -> bytes:
        pass
