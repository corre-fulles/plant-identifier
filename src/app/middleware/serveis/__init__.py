from .servei_api_identificacio import ApiIServeiIdentificacio
from .service_locator import ServiceLocator, FrameworkAgentFactoryLocator, ServeiIdentificacioLocator
