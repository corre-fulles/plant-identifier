from abc import abstractmethod
from typing import List

from app.config import MAXIM_OPCIONS, TIMEOUT_PETICIONS,IMATGES_HABILITADES
from app.domain.dto import OpcioIdentificacioDTO
from app.interfaces.serveis import IServeiIdentificacio


class ApiIServeiIdentificacio(IServeiIdentificacio):
    def __init__(self):
        self.maxim_opcions = MAXIM_OPCIONS
        self.timeout = TIMEOUT_PETICIONS
        self.inclou_imatges = IMATGES_HABILITADES

    @abstractmethod
    def troba_opcions(self, foto) -> List[OpcioIdentificacioDTO]:
        raise NotImplementedError()
