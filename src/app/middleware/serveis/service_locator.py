from typing import Type

from app.interfaces.serveis import IServeiIdentificacio, Servei, ServeiIdentificador
from app.middleware.framework import AbstractFrameworkAgentFactory, FrameworkAgentFactory
from app.utils import implementacions


class ServiceLocatorError(Exception):
    def __init__(self, message=""):
        super().__init__(f"Error del localitzador: {message}.")


class ServeiNoImplementat(ServiceLocatorError):
    def __init__(self, classe_servei: Type):
        super().__init__(f"La interfície, servei o classe abstracta {classe_servei.__name__} no té implementacions")


class ServeiNoRegistrat(ServiceLocatorError):
    def __init__(self, id_servei: str):
        super().__init__(f"No existeix cap servei registrat amb ID {id_servei}")


class ServiceLocator:
    def __init__(self, classe_servei):

        implementacions_servei = implementacions(classe_servei)
        if len(implementacions_servei) == 0:
            raise ServeiNoImplementat(classe_servei)
        self.registre_serveis = {
            servei.id_servei(): servei for servei in implementacions(classe_servei)
        }

    def find(self, id_servei: str, *args, **kwargs) -> Servei:
        if id_servei in self.registre_serveis:
            crear_servei = self.registre_serveis.get(id_servei)
            return crear_servei(*args, **kwargs)
        else:
            raise ServeiNoRegistrat(id_servei)


class ServeiIdentificacioLocator(ServiceLocator):
    def __init__(self):
        super().__init__(IServeiIdentificacio)
        print("Identificadors registrats")

    def find(self, id_servei: str, *args, **kwargs) -> ServeiIdentificador:
        return super().find(id_servei, *args, **kwargs)


class FrameworkAgentFactoryLocator(ServiceLocator):
    def __init__(self):
        super().__init__(AbstractFrameworkAgentFactory)
        print("Frameworks registrats")

    def find(self, id_servei: str, *args, **kwargs) -> FrameworkAgentFactory:
        return super().find(id_servei, *args, **kwargs)
