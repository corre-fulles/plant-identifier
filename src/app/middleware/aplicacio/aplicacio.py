from app.middleware.framework import FrameworkAgent
from app.middleware.serveis import FrameworkAgentFactoryLocator, ServeiIdentificacioLocator


class Aplicacio:

    def __init__(self, agent: FrameworkAgent):
        self.agent = agent

    def run(self, host=None, port=None, **kwargs):
        self.agent.iniciar(host, port, **kwargs)


class AplicacioFactory:

    def __init__(
            self,
            frameworks: FrameworkAgentFactoryLocator,
            identificadors: ServeiIdentificacioLocator):

        self.frameworks = frameworks
        self.identificadors = identificadors

    @property
    def ids_identificadors(self):
        return list(self.identificadors.registre_serveis.keys())

    @property
    def ids_frameworks(self):
        return list(self.frameworks.registre_serveis.keys())

    def crear_aplicacio(self, nom_agent, nom_servei, *args, **kwargs) -> Aplicacio:
        try:
            agent_factory = self.frameworks.find(nom_agent)
            servei = self.identificadors.find(nom_servei)
            agent = agent_factory.crear_agent(servei, *args, **kwargs)
            return Aplicacio(agent)

        except Exception as e:
            raise SystemError(str(e))
