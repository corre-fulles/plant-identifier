import os 
from distutils.util import strtobool

FORMATS_VALIDS = (
    'image/png'
)
MIDA_MAXIMA_IMATGE = 50 * 1024


IDIOMA_BASE = os.environ.get('LANGUAGE', 'es')
IMATGES_HABILITADES = bool(os.environ.get('USE_IMAGES',"0"))
MAXIM_OPCIONS = 3
TIMEOUT_PETICIONS = 5
