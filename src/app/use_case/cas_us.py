from abc import ABC, abstractmethod


class ComandaCasUs(ABC):
    @abstractmethod
    def executa(self, *args, **kwargs):
        raise NotImplementedError


class CasUs(ABC):
    def __init__(self,  comanda: ComandaCasUs):
        self.comanda = comanda

    @abstractmethod
    def executa(self, *args, **kwargs):
        raise NotImplementedError
