from typing import List

from app.domain import Foto, Identificacio, OpcioIdentificacio, OpcioIdentificacioDTO

from .cas_us import CasUs, ComandaCasUs
from app.interfaces.serveis import IServeiIdentificacio

class ComandaIdentificaPlanta(ComandaCasUs):

    def __init__(self, adaptador: IServeiIdentificacio):
        self.adaptador_remot = adaptador

    def executa(self, foto: Foto) -> List[OpcioIdentificacioDTO]:
        return self.adaptador_remot.troba_opcions(foto)

class IdentificaPlantaUseCase(CasUs):

    def __init__(self, comanda: ComandaIdentificaPlanta):
        super().__init__(comanda)

    def executa(self, foto: Foto) -> Identificacio:
        elements_api = self.comanda.executa(foto)
        opcions_trobades = [
            OpcioIdentificacio.fromDTO(element) for element in elements_api
        ]
        return Identificacio(opcions_trobades)




