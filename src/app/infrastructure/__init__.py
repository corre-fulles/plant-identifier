
from .serveis import PillowFotoParser, PlantnetApiServeiIdentificador
from .framework import FlaskAgent, FlaskIdentificacioController, FlaskAgentFactory, FalconAgent, \
    FalconIdentificacioController, FalconAgentFactory
from .dependencies import app_factory
