from typing import List

from app.config import IMATGES_HABILITADES
from app.domain import OpcioIdentificacioDTO
from app.middleware.serveis import ApiIServeiIdentificacio


class ExempleApiServeiIdentificacio(ApiIServeiIdentificacio):

    def troba_opcions(self, foto) -> List[OpcioIdentificacioDTO]:
        exemple = {
            'nom_cientific': 'Nom científic',
        }
        if IMATGES_HABILITADES:
            exemple['imatge_referencia'] = {
                'url': 'https://algun.cdn.org/exemple.png',
                'acreditacio': {
                    'autor': 'autoria',
                    'llicencia': 'acrònim o nom de llicència',
                    'cita': 'text amb autor , llicència i més',
                    'timestamp_creacio': 'timestamp (format numeric)'
                }
            }
        return [exemple]

    @classmethod
    def id_servei(cls) -> str:
        return "exemple"
