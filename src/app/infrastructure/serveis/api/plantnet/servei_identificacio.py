import os
from functools import partial
from typing import List

import requests

from app.config import IDIOMA_BASE
from app.domain import Foto, OpcioIdentificacioDTO
from app.middleware.errors import OpcionsNoTrobadesError, ApiError, OpcionsMalParsejadesError, TempsLimitExceditError
from app.middleware.serveis import ApiIServeiIdentificacio
from app.infrastructure.serveis.api.transformers import PlantnetDTOTransformer


class PlantnetApiServeiIdentificador(ApiIServeiIdentificacio):
    BASE_URL = os.environ.get('PLANTNET_API_URL')
    VERSIO = os.environ.get('PLANTNET_API_VERSION')
    PROJECTE = os.environ.get('PLANTNET_API_PROJECT')

    def __init__(self):
        super().__init__()
        self.url = f"{self.BASE_URL}/{self.VERSIO}/identify/{self.PROJECTE}"

    @classmethod
    def id_servei(cls) -> str:
        return 'plantnet'

    def troba_opcions(self, foto: Foto) -> List[OpcioIdentificacioDTO]:
        try:
            temp_filename = foto.nom
            payload = [("images", (temp_filename, foto.contingut, f"image/{foto.format_imatge.value}"))]
            parametres = {
                    'lang': IDIOMA_BASE, 'type': 'kt',
                    'include-related-images': self.inclou_imatges,
                    'api-key': os.environ.get('PLANTNET_API_KEY')
            }
            response = requests.post(
                self.url, files=payload, data={'organs': 'auto'},
                params=parametres, timeout=self.timeout)

            if response.status_code != requests.codes.ok:
                cos_json_error = response.json()
                if cos_json_error["statusCode"] == 404:
                    raise OpcionsNoTrobadesError(
                        "No s'ha reconegut cap espècie a la foto enviada")
                raise ApiError(
                    f"Error HTTP {cos_json_error['statusCode']} : {cos_json_error['message']}")

            cos_json_resposta = response.json()
            opcions = cos_json_resposta.get('results', None)
            if opcions is None:
                raise OpcionsNoTrobadesError(
                    "No s'ha reconegut cap espècie a la foto enviada")

            opcions.sort(key=lambda x: x["score"], reverse=True)
            millors_n_opcions = opcions[slice(self.maxim_opcions)]
            parseja_dto = partial(PlantnetDTOTransformer.crea_dto_de_dades, inclou_imatges=self.inclou_imatges)
            opcions = list(map(parseja_dto, millors_n_opcions))
            return opcions
        except OpcionsMalParsejadesError as error_parseig:
            raise error_parseig
        except ApiError as error_api:
            raise error_api
        except requests.Timeout:
            raise TempsLimitExceditError()
        except requests.JSONDecodeError as error_json:
            raise ApiError(f"Error processant la resposta: {str(error_json)}")
        except Exception as error:
            raise ApiError(f"Error deconegut al servei d'API: {str(error)}")
