class PlantnetDTOTransformer(ApiDTOTransformer):
    LLICENCIES_VALIDES = ['public', 'cc-by-sa']

    @classmethod
    def dto_from_data(cls, data: dict, inclou_imatges: bool = False) -> OpcioIdentificacioDTO:

        def imatge_referencia_dto_from_data(imatges: List[Dict] = None):
            def te_llicencia_valida(imatge_item: Dict) -> bool:
                return imatge_item.get('license', None) in cls.LLICENCIES_VALIDES

            if inclou_imatges and imatges is not None and len(imatges) > 0:
                primera_imatge_valida = primer_element_filtrat(te_llicencia_valida, imatges)
                if primera_imatge_valida:
                    return {
                        'url': primera_imatge_valida['url']['m'],
                        'acreditacio': {
                            'autor': primera_imatge_valida['author'],
                            'llicencia': primera_imatge_valida['license'],
                            'cita': primera_imatge_valida['citation'],
                            'timestamp_creacio': primera_imatge_valida['date']['timestamp']
                        }
                    }
            else:
                return None
        try:
            opcio_parsejada = {'nom_cientific': data['species']['scientificNameWithoutAuthor']}
            imatge_referencia = imatge_referencia_dto_from_data(data.get('images', None))
            if imatge_referencia:
                opcio_parsejada['imatge_referencia'] = imatge_referencia
            return opcio_parsejada

        except Exception as error:
            raise OpcionsMalParsejadesError(
                f"Error processant les dades de la resposta de l'API externa: {str(error)}")