from io import BytesIO

from PIL import Image, UnidentifiedImageError

from app.middleware.errors import FotoTruncadaError, FotoBuidaError
from app.middleware.serveis.foto_parser import FotoParser


class PillowFotoParser(FotoParser):

    @staticmethod
    def _extreu_bytes(arxiu_imatge) -> bytes:
        try:

            image = Image.open(BytesIO(arxiu_imatge.stream.read()), formats=['png'])
            buffer_io_imatge = BytesIO()
            image.save(buffer_io_imatge, format='PNG', optimize=True)
            bytes_imatge = buffer_io_imatge.getvalue()
            if len(bytes_imatge) == 0:
                raise FotoBuidaError()
            return bytes_imatge
        except UnidentifiedImageError as error_open:
            raise FotoTruncadaError()


