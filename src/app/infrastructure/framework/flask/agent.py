from flask import Flask as FlaskApp

from app.interfaces.serveis import IServeiIdentificacio
from app.middleware.framework import AbstractFrameworkAgentFactory, FrameworkAgent
from .controller import FlaskIdentificacioController
from .wrapper import FlaskAppWrapper


class FlaskAgent(FrameworkAgent):

    def _inicialitzar_rutes(self):
        self.app_wrapper.afegir_ruta('/identificar/', ['POST'], self.controlador.identificar_imatge)

    def iniciar(self, host="localhost", port=8050, debug=True, ssl=False, **kwargs):
        self.app_wrapper.run(host=host, port=port, debug=debug, ssl=ssl, **kwargs)


class FlaskAgentFactory(AbstractFrameworkAgentFactory):
    @classmethod
    def id_servei(cls):
        return 'flask'

    @classmethod
    def crear_agent(cls, servei_identificador: IServeiIdentificacio, *args, **kwargs) -> FrameworkAgent:
        return FlaskAgent(
            FlaskAppWrapper(FlaskApp(*args, import_name="identificador", **kwargs)),
            FlaskIdentificacioController(servei_identificador)
        )
