from flask import request, jsonify

from app.use_case import IdentificaPlantaUseCase, ComandaIdentificaPlanta
from app.middleware.controller import AbstractIdentificacioController
from app.middleware.errors.errors import ControllerError, FormatArxiuIncorrecteError, ArxiuNoInclosError, \
    FotoParserError
from app.infrastructure.serveis.foto_parser.pillow import PillowFotoParser


class FlaskIdentificacioController(AbstractIdentificacioController):

    def identificar_imatge(self):
        try:
            if 'arxiu_imatge' in request.files:
                arxiu = request.files['arxiu_imatge']
                if arxiu:
                    if not self._es_format_valid(arxiu.content_type):
                        raise FormatArxiuIncorrecteError(arxiu.content_type)
                    foto = PillowFotoParser.foto_from_arxiu(arxiu)
                    arxiu.close()
                    del arxiu
                    comanda = ComandaIdentificaPlanta(self.adaptador_api)
                    cas_us = IdentificaPlantaUseCase(comanda)
                    resultat = cas_us.executa(foto)
                    resultat_json = resultat.toJSON()
                    opcions_resultat = resultat_json.get('opcions', [])
                    return jsonify(status='OK', resultats=opcions_resultat), 200
                else:
                    raise ArxiuNoInclosError('L\'arxiu no es troba al camp "arxiu_imatge"')
            else:
                raise ArxiuNoInclosError()
        except FotoParserError as error_parseig:
            return jsonify(status='KO', error=str(error_parseig)), 400
        except ControllerError as error_controller:
            return jsonify(status='KO', error=str(error_controller)), 400
        except Exception as err:
            return jsonify(status='KO', error=str(err)), 500
