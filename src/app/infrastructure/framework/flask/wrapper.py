from flask import Flask

from app.middleware.framework import FrameworkAppWrapper


class FlaskAppWrapper(FrameworkAppWrapper):

    def __init__(self, aplicacio: Flask):
        super().__init__(aplicacio)

    def afegir_ruta(self, url: str, metodes: list, handler):
        self.app.add_url_rule(url, methods=metodes, view_func=handler)

    def run(self, host: str, port: int, debug=True, ssl=False, **kwargs):
        # print(debug)
        if debug:
            self.app.run(host=host, port=port, debug=True, ssl_context="adhoc" if ssl else None, **kwargs)
        else:
            from werkzeug.middleware.proxy_fix import ProxyFix
            from waitress import serve
            self.app.wsgi_app = ProxyFix(self.app.wsgi_app,x_for=1, x_proto=1, x_host=1)
            serve(self.app, url_scheme='https' if ssl else None, host=host, port=port,**kwargs)

        
