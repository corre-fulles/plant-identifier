import falcon
from werkzeug import run_simple

from app.middleware.framework import FrameworkAppWrapper


class FalconAppWrapper(FrameworkAppWrapper):

    def __init__(self, aplicacio: falcon.App):
        super().__init__(aplicacio)

    def afegir_ruta(self, url: str, metodes: list, handler, **kwargs):
        self.app.add_route(url, handler, **kwargs)

    def run(self, host: str, port: int, debug=True, ssl=False, **kwargs):
        if debug:
            run_simple(host, port, self.app, use_debugger=debug, ssl_context='adhoc' if ssl else None, **kwargs)
        else:
            from werkzeug.middleware.proxy_fix import ProxyFix
            from waitress import serve
            serve(
                ProxyFix(self.app, x_for=1, x_proto=1, x_host=1),
                url_scheme='https' if ssl else None,
                host=host,
                port=port,
                **kwargs)
