
from falcon import App as FalconApp

from app.interfaces.serveis import IServeiIdentificacio
from app.middleware.framework import AbstractFrameworkAgentFactory, FrameworkAgent
from .wrapper import FalconAppWrapper
from .controller import FalconIdentificacioController


class FalconAgent(FrameworkAgent):

    def _inicialitzar_rutes(self):
        self.app_wrapper.afegir_ruta('/identificar/', ['POST'], self.controlador, suffix='identificar')

    def iniciar(self, host="localhost", port=8050, debug=True, ssl=False, **kwargs):
        self.app_wrapper.run(host=host, port=port, debug=debug, ssl=ssl, **kwargs)


class FalconAgentFactory(AbstractFrameworkAgentFactory):
    @classmethod
    def id_servei(cls) -> str:
        return 'falcon'

    @classmethod
    def crear_agent(cls, servei: IServeiIdentificacio, *args, **kwargs) -> FrameworkAgent:
        return FalconAgent(
            FalconAppWrapper(FalconApp(*args, **kwargs)),
            FalconIdentificacioController(servei)
        )