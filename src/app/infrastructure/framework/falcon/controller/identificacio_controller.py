from falcon.request import Request as FalconRequest
from falcon.response import Response as FalconResponse
from falcon.status_codes import HTTP_200, HTTP_400, HTTP_500

from app.infrastructure.serveis import PillowFotoParser
from app.middleware.controller import AbstractIdentificacioController
from app.middleware.errors import ControllerError, ArxiuNoInclosError, FormatArxiuIncorrecteError, FotoParserError
from app.use_case import IdentificaPlantaUseCase, ComandaIdentificaPlanta
from app.utils import primer_element_filtrat


class FalconIdentificacioController(AbstractIdentificacioController):

    def identificar_imatge(self, req: FalconRequest):
        if req.stream is None:
            raise ArxiuNoInclosError()
        multipart_form_iterator = req.get_media()
        part_arxiu = primer_element_filtrat(
            lambda part: part.name == 'arxiu_imatge',
            multipart_form_iterator)
        if part_arxiu is None:
            raise ArxiuNoInclosError('L\'arxiu no es troba al camp "arxiu_imatge"')
        if not self._es_format_valid(part_arxiu.content_type):
            return FormatArxiuIncorrecteError(part_arxiu.content_type)
        foto = PillowFotoParser.foto_from_arxiu(part_arxiu)
        comanda = ComandaIdentificaPlanta(self.adaptador_api)
        resultat_identificacio = IdentificaPlantaUseCase(comanda).executa(foto)
        json_resultat_identificacio = resultat_identificacio.toJSON()
        llista_opcions = json_resultat_identificacio.get('opcions', [])
        return llista_opcions

    def on_post_identificar(self, req: FalconRequest, resp: FalconResponse):
        try:
            opcions_identificacio = self.identificar_imatge(req)
            resp.content_type = 'application/json'
            resp.status = HTTP_200
            resp.media = {'status': 'OK', 'resultats': opcions_identificacio}
        except FotoParserError as error_parseig:
            resp.status = HTTP_400
            resp.content_type = 'application/json'
            resp.media = {'status': 'KO', 'error': str(error_parseig)}
        except ControllerError as error_controlador:
            resp.status = HTTP_400
            resp.content_type = 'application/json'
            resp.media = {'status': 'KO', 'error': str(error_controlador)}
        except Exception as err:
            resp.status = HTTP_500
            resp.content_type = 'application/json'
            resp.media = {'status': 'KO', 'error': str(err)}
