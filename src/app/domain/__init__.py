from .foto import Foto
# from .factory import FotoFactory
from .identificacio import OpcioIdentificacio, Identificacio
from .error import DomainError
from .dto import OpcioIdentificacioDTO