import json
import re
from typing import List, Dict

from app.domain.dto import OpcioIdentificacioDTO

AcreditacioImatge = Dict[str, str]

class ImatgeReferencia:

    def __valida_url(self, text_url:str):
        pattern = re.compile(r"^(http|https)://[^\s/$.?#].[^\s]*$")
        if not re.match(pattern, text_url):
            raise ValueError


    def __init__(self, url: str, acreditacio: AcreditacioImatge = None):
        self.__valida_url(url)
        self.url = url
        self.acreditacio = acreditacio if (acreditacio and len(acreditacio) > 0) else None

    @classmethod
    def fromDTO(cls, json_dto: Dict):
        return cls(url=json_dto['url'], acreditacio=json_dto.get('acreditacio', None))

    def toJSON(self):
        return {
            'url': self.url,
            'acreditacio': self.acreditacio
        } if self.acreditacio else {'url': self.url}


class OpcioIdentificacio(object):

    def __init__(self, nom_cientific: str = "", imatge_referencia: ImatgeReferencia = None):
        self.nom_cientific = nom_cientific
        self.imatge_referencia = imatge_referencia

    @classmethod
    def fromDTO(cls, dto: OpcioIdentificacioDTO):
        try:
            nom_cientific = dto['nom_cientific']
            nova_opcio = cls(nom_cientific=nom_cientific)
            if 'imatge_referencia' in dto:
                nova_opcio.imatge_referencia = ImatgeReferencia.fromDTO(dto['imatge_referencia'])
            return nova_opcio
        except Exception as error:
            raise Exception(f"Error parsejant json: {dto} : {str(error)}")

    def toJSON(self):
        diccionari_json = {
            'nom_cientific': self.nom_cientific,
        }
        if self.imatge_referencia:
            diccionari_json['imatge_referencia'] = self.imatge_referencia.toJSON()
        return diccionari_json


class Identificacio(object):

    def __init__(self, opcions: List[OpcioIdentificacio] = None):
        self.opcions = opcions or []

    def toJSON(self):
        return {
            'opcions': [opcio.toJSON() for opcio in self.opcions]
        }
