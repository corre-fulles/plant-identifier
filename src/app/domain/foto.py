from enum import Enum

from .error import DomainError


class Foto(object):
    class Format(Enum):
        PNG = 'png'

    @classmethod
    def formats_valids(cls):
        return [element.value for element in cls.Format]

    def __init__(self, nom, contingut: bytes, format_imatge: str):
        try:
            if format_imatge is None:
                raise ValueError('Cal especificar el format de la foto')
            self.format_imatge = self.Format[format_imatge.upper()]
            if contingut is None or len(contingut) == 0:
                raise ValueError('El contigut es buit')
            self.nom = nom
            self.contingut = contingut
        except KeyError:
            raise DomainError(f"Format '{format_imatge}' no suportat.")
        except ValueError as ve:
            raise DomainError(str(ve))
        except Exception as e:
            raise DomainError(f'Error desconegut: {str(e)}')
