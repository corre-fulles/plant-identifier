
class DomainError(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)
        self.missatge = super().__str__()

    def __str__(self) -> str:
        return f"[DomainError] {self.missatge}"
