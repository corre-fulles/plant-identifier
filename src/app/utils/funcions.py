import inspect
from functools import partial
from typing import Callable, Iterable, Optional, Set

from .tipus import T


def primer_element_filtrat(condicio: Callable[[T], bool], elements: Iterable[T]) -> Optional[T]:
    # funcio_filtre = partial(condicio)
    first_element = next(filter(condicio, elements), None)
    return first_element


def implementacions(classe) -> Set:
    classes = set() if inspect.isabstract(classe) else {classe}
    subclasses = classe.__subclasses__()
    classes.update(*[implementacions(subclasse) for subclasse in subclasses])
    return classes
