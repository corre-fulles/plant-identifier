# Identificador de plantes

Microservei que identifica plantes

## Com executar
1. Revisar l'arxiu run_server.sh . Establir quins parametres d'entrada es decideixen desde l'script
2. Crear un arxiu .env amb les següents variables d'entorn

| Nom                | Tipus     | Descripció                                                                                      |
|--------------------|-----------|-------------------------------------------------------------------------------------------------|
| USE_IMAGES         | int [0,1] | Indica al sistema que, quan sigui possible, adjunti a cada opció una imatge (url) de referència |
| \<SERVEI\>_API_URL | url       | Url base o complerta de l'API externa de reconeixement de plantes                               |
|                    |           |                                                                                                 |
|                    |           |                                                                                                 |
|                    |           |                                                                                                 |



````commandline
docker-compose up
````